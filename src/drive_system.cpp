#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/PoseStamped.h"
#include "vector"
#include "VectorPolar.h"
//#include <math.h>

/*
 * This is a code that was used for trying to solve the Box Canyon Problem. Unfortunately it only worked on stage.
 * The intend is to add more weight to the repulsive force when the robot goes back to a previous visited position.
 * Source: Potential Fields Tutorial - Michael A. Goodrich.

struct comparePositions
{
  bool operator()(tf::StampedTransform s1,tf::StampedTransform s2) const
  {	bool result = false;
  	double margin = 0.05;

    if(!(s1.getOrigin().getX() <= s2.getOrigin().getX() + margin && s1.getOrigin().getX() >= s2.getOrigin().getX() - margin
    		&& s1.getOrigin().getY() <= s2.getOrigin().getY() + margin && s1.getOrigin().getY() >= s2.getOrigin().getY() - margin
    		&& s1.getRotation().getAngle() <= s2.getRotation().getAngle() +  margin && s1.getRotation().getAngle() >= s2.getRotation().getAngle() -  margin)){
    	result = true;
    }

    return result;
  }
};

*/

class DriveSystem {

private:
	ros::NodeHandle nodeHandle_;
	ros::Subscriber scanSubscriber_;
	ros::Subscriber goalSubscriber_;
	ros::Publisher publisher_;
	tf::TransformListener listener_;
	std::vector<VectorPolar> listOfVectors;
	tf::TransformBroadcaster br;
	geometry_msgs::PoseStamped goalMsg_;
	bool gotGoal;
	//typedef std::map<tf::StampedTransform, int, comparePositions> mapPositions;
	//mapPositions locations;

	/* Parameters  */
	std::string scanNodeId;
	double safeDistance;
	double theta;
	double alpha;
	double beta;
	double epsilon;
	double angle_weight;
	double max_force;
	double max_speed;
	double goal_radios;

public:
	DriveSystem(ros::NodeHandle &nodeHandle) {
		nodeHandle_ = nodeHandle;

		setupParameter();
		scanSubscriber_ = nodeHandle_.subscribe(scanNodeId, 1000, &DriveSystem::updateCallback, this);
		goalSubscriber_ = nodeHandle_.subscribe("move_base_simple/goal", 1000, &DriveSystem::goalCallback, this);
		publisher_ = nodeHandle_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);

	}

	/*
	 * Method to setup parameters to the algorithms.
	 *
	 */
	void setupParameter(){
		scanNodeId = "narrow_scan";
		//scanNodeId = "base_scan";

		gotGoal = false; // boolean for waiting the goal to be set before start moving robot.

		safeDistance = 1.2;
		theta = 30;
		alpha = 3;
		beta = 4;
		epsilon = 0.05;


		angle_weight = 2;

		// a constant for the maximum force the directionVector can achieved. It is used to calculate the velocity.
		max_force = 60000;
		max_speed = 0.5;

		goal_radios = 4;

	}

	/*
	 * Method to get the goal from the subscriber listen for the rviz node.
	 */
	void goalCallback(const geometry_msgs::PoseStamped goalMsg) {
		goalMsg_ = goalMsg;
		gotGoal = true;
	}

	/*
	 * This method is used to publish the goal vector that comes from the rviz node to tf.
	 */
	void publishGoaltoTF(){
		tf::Stamped<tf::Transform> transform;
		tf::poseStampedMsgToTF(goalMsg_,transform);
		br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "my_goal"));
	}

	/*
	 * This method is used to publish vector to tf for debugging on rviz.
	 */
	void publishVector(VectorPolar vector, std::string parentId, std::string childId){

		double vector1X = vector.getDistance() * cos(vector.getAngle());
		double vector1Y = vector.getDistance() * sin(vector.getAngle());

		tf::Transform transform;
		transform.setOrigin( tf::Vector3(vector1X, vector1Y, 0.0) );
		transform.setRotation( tf::Quaternion(vector.getAngle(), 0, 0) );
		br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parentId, childId));
	}


	/*
	 * This is the callback for laser subscriber.
	 * It will take the distances and the angles of the laser vectors.
	 */
	void updateCallback(const sensor_msgs::LaserScan::ConstPtr& msg) {

		double numberOfRanges = msg->ranges.size();
		int x;

		listOfVectors.clear();
		for (x = 0; x <= numberOfRanges; x++) {
			double range = msg->ranges[x];

			if(range !=0){
				double angle = (double(x) * msg->angle_increment) + msg->angle_min;
				VectorPolar vectorPolar(range, angle);
				listOfVectors.push_back(vectorPolar);
			}
		}
	}

	/*
	 * This method is used to calculate the repulsive vector.
	 * It also publishes the repulsive vector to tf.
	 */
	VectorPolar getRepulsiveVector(){
		std::vector<VectorPolar> listOfInvertedVectors;
		VectorPolar repulsiveVector = VectorPolar(0,0);

		if(listOfVectors.size() > 0){

			for (unsigned i=0; i<listOfVectors.size(); i++) {
				VectorPolar invertedVector =  getInverted(listOfVectors.at(i));
				listOfInvertedVectors.push_back(invertedVector);
			}

			VectorPolar vectorSum = VectorPolar(0,0);
			for (unsigned i=0; i<listOfInvertedVectors.size(); i++) {
				double force = getForce(listOfInvertedVectors.at(i).getDistance());
				VectorPolar forceVector = VectorPolar(force,listOfInvertedVectors.at(i).getAngle());
				vectorSum = sumVectors(vectorSum,forceVector);
			}
			repulsiveVector = vectorSum;
		}
		publishVector(repulsiveVector, "base_link", "repulsive");
		return repulsiveVector;
	}

	VectorPolar getInverted(VectorPolar originalVector){
		double invertedDistance = originalVector.getDistance();
		double invertedAngle =  -1 * originalVector.getAngle();
		VectorPolar vectorPolarInverted(invertedDistance, invertedAngle);
		return vectorPolarInverted;
	}


	/*
	 * This method does a polar sum of two vectors.
	 */
	VectorPolar sumVectors(VectorPolar vector1, VectorPolar vector2){
		double vector1X = vector1.getDistance() * cos(vector1.getAngle());
		double vector1Y = vector1.getDistance() * sin(vector1.getAngle());

		double vector2X = vector2.getDistance() * cos(vector2.getAngle());
		double vector2Y = vector2.getDistance() * sin(vector2.getAngle());

		double resultantX = vector1X + vector2X;
		double resultantY = vector1Y + vector2Y;

		double resultant = sqrt(pow(resultantX,2) + pow(resultantY,2));

		double angle = atan2(resultantY,resultantX);

		VectorPolar vectorXY(resultant, angle);

		return vectorXY;
	}

	/*
	 * This method implements the calculation of the force.
	 *
	 */
	double getForce(double distance){

		double force = 0;

		if(distance > (safeDistance + epsilon) && distance < beta ){

			force = alpha / pow(distance - safeDistance,2);

		}else if(distance < (safeDistance + epsilon)){

			force = alpha / pow(epsilon,2);
		}

		return  abs(force);
	}

	void drive(){

		geometry_msgs::Twist base_cmd;
		base_cmd.linear.y = 0;

		tf::StampedTransform transform;
		tf::StampedTransform transformGoal;
		tf::StampedTransform transformRepulsive;

		double x = 0;
		double z = 0;

		ros::Rate rate(10.0);
		bool done = false;
		ROS_INFO("Drive system started and waiting for goal. Please set a goal on RVIZ.");
		while (!done && nodeHandle_.ok())
		{
			if(gotGoal == true){
				publishGoaltoTF();

				getRepulsiveVector();

				try{

					 listener_.lookupTransform("/odom", "/base_link", ros::Time(0), transform);

					 listener_.lookupTransform("/base_link", "/repulsive", ros::Time(0), transformRepulsive);

					 listener_.lookupTransform("/base_link", "/my_goal", ros::Time(0), transformGoal);
				 }
				 catch (tf::TransformException ex){
					 ROS_ERROR("%s",ex.what());
				 }

				double resultantX = transformGoal.getOrigin().x();
				double resultantY = transformGoal.getOrigin().y();

				double resultantRX = transformRepulsive.getOrigin().x();
				double resultantRY = transformRepulsive.getOrigin().y();


				VectorPolar attractionVector(theta * (sqrt(pow(resultantX, 2)) + pow(resultantY, 2)), atan2(resultantY,resultantX));
				publishVector(attractionVector, "base_link", "attraction");


				VectorPolar repulsiveVector(sqrt(pow(resultantRX, 2) + pow(resultantRY, 2)), atan2(resultantRY,resultantRX));
				publishVector(repulsiveVector, "base_link", "repulsiveVector");


				VectorPolar directionVector = sumVectors(repulsiveVector,attractionVector);
				publishVector(directionVector, "base_link", "directionVector");


				/*
				 *	This is a code was used for trying to solve the Box Canyon Problem.
				 *
				 *
				 std::pair<std::map<tf::StampedTransform,int>::iterator,bool> ret;
				 ret = locations.insert(( std::pair<tf::StampedTransform,int>(transform,1) ));
				 if (ret.second==false) {
					ret.first->second = ret.first->second + 30;
					VectorPolar pastPositionVector(ret.first->second, 0);
					 VectorPolar directionVector = sumVectors(directionVector,pastPositionVector);
					std::cout << "element already existed";
					std::cout << " with a value of " << ret.first->second << '\n';
				 }
				 ROS_INFO("locations[transform]: [%d]", locations.size());
				 ROS_INFO("ret.first->second: [%d]", ret.first->second);

				 */


				// the angle velocity is set to resultant force vector - the robot rotation.
				z = angle_weight * (directionVector.getAngle() - tf::getYaw(transform.getRotation()));

				/*
				 * The below is the code responsible to calculate the velocity. This is based on a normalization of the resultant vector force to an interval from 0 to 0.5.
				 * I also watches the attraction force to know when the robot arrives to the goal, consequently,  stopping it.
				 *
				 */
				if(attractionVector.getDistance() > 0){

					if(attractionVector.getDistance() < goal_radios){
						x = 0;
						z = 0;
						gotGoal = false;
						ROS_INFO("************************************ ARRIVED ************************************");
						//locations.clear();
					}else{
						if(directionVector.getDistance() != 0){
							double normal = -((directionVector.getDistance() - 0) / (max_force - 0) * (max_speed - 0)) + max_speed;
							if(normal < 0){
								x = 0;
							}else{
								x = normal;
							}
						}else{
							x = max_speed;
						}
					}
				}


				ROS_INFO("attractionVector.distance: [%f]", attractionVector.getDistance());
				ROS_INFO("repulsiveVector.getDistance(): [%f]", repulsiveVector.getDistance());
				ROS_INFO("directionVector.distance: [%f]", directionVector.getDistance());
				ROS_INFO("directionVector.angle: [%f]", directionVector.getAngle());
				ROS_INFO("Velocity: [%f]", x);



				base_cmd.linear.x = x;
				base_cmd.angular.z = z;

				//send the drive command
				publisher_.publish(base_cmd);

			}
			ros::spinOnce();
			rate.sleep();
			//get the current transform

		}
	}

};


int main(int argc, char **argv) {

	ros::init(argc, argv, "drive_system");

	ros::NodeHandle nodeHandler;

	DriveSystem driveSystem(nodeHandler);
	driveSystem.drive();
}


